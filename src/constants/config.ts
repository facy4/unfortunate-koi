export const config = {
	baseURL: '/api',
	encryptKey: process.env.VUE_APP_ENCRYPTKEY || '',
	websocketUrl: process.env.NODE_ENV === 'production' ? 'wss://facy.top/websocket/' : 'ws://192.168.1.102:9998/'
};
