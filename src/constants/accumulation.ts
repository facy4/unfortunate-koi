export enum AccumulationType {
	Code = 'Code',
	Concept = 'Concept',
	Personal = 'Personal'
}
export const accumulationOptions = [
	{label: '个人经验', value: AccumulationType.Personal},
	{label: '概念题', value: AccumulationType.Concept},
	{label: '代码题', value: AccumulationType.Code}
];
export interface IAccumulation {
	id: number;
	title: string;
	author: string;
	type: string;
	like_num: string;
	like_user: string;
	content: string;
	like_users: string[];
	brief: number;
}
