import {RouteFormConfig} from '@/constants';
const needDEncryptApiApiList = ['/user/login'];
const defaultEncryptApiList = ['/api/getAllApis'];
const defaultAuthApiList = ['/accumulation/changeAccumulationLikeState'];
export const initState: {
	userinfo: {
		token: string;
		username: string;
		userid: string;
		loginState: boolean;
	};
	iv: string;
	showMenu: boolean;
	menuList: RouteFormConfig[];
	needDEncryptApiList: string[];
	needEncryptApiList: string[];
	needAuthApiList: string[];
	ipConfigInfo: {
		city: string;
		ip: string;
		addr: string;
	};
} = {
	userinfo: {
		token: '',
		username: '',
		userid: '',
		loginState: false
	},
	iv: '',
	showMenu: false,
	menuList: [],
	needDEncryptApiList: needDEncryptApiApiList,
	needEncryptApiList: defaultEncryptApiList,
	needAuthApiList: defaultAuthApiList,
	ipConfigInfo: {
		city: '',
		ip: '',
		addr: ''
	}
};
