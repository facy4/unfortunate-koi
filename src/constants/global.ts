export enum paramsType {
	Array = '[object Array]',
	Object = '[object object]',
	String = '[object String]',
	Number = '[object Number]',
	Undefined = '[object Undefined]',
	Boolean = '[object Boolean]',
	Null = '[object Null]',
	FormData = '[object FormData]'
}
export interface ResponseProps {
	data: any;
	msg?: string;
	code?: number;
	headers?: any;
	fileName?: string;
}
export interface NormalObject {
	[key: string]: string | number | boolean;
}
export interface AnyObject {
	[key: string]: any;
}
export enum ProcessState {
	Pendding = 'pendding',
	Fullfilled = 'fullfilled',
	Rejected = 'rejected'
}
export enum WindowEx {
	ClassicEditor = 'ClassicEditor'
}
