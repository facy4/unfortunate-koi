import {isEqual, isEmpty} from '@/utils';
export enum RuleTypes {
	BASE = '-a-zA-Z0-9_',
	zh = '\\u4E00-\\u9FA5',
	Number = '\\d',
	Slash = '|\\/'
}

export enum RulesStr {
	BASE = '大小写英文字母',
	zh = '中文',
	Number = '数字',
	Slash = '斜杠'
}
export const getRule: (min: number, max: number, ...args: string[]) => {reg: string | RegExp; message: string} = (
	min,
	max,
	...args
) => {
	const ruleObj = args.reduce(
		(pre, next) => {
			pre.reg += next;
			Object.entries(RuleTypes).forEach((item: string[]) => {
				const [key, value] = item;
				if (isEqual(next, value)) {
					if (isEmpty(pre.message)) {
						pre.message += '仅支持';
					}
					switch (key) {
						case 'BASE':
							pre.message += RulesStr.BASE;
							break;
						case 'zh':
							pre.message += RulesStr.zh;
							break;
						case 'Number':
							pre.message += RulesStr.Number;
							break;
						case 'Slash':
							pre.message += `${RulesStr.Slash}`;
							break;
					}
				}
			});
			return pre;
		},
		{reg: '', message: ''} as {reg: string | RegExp; message: string}
	);
	const ruleReg = new RegExp(`^([${ruleObj.reg}]){${min},${max}}$`);
	ruleObj.reg = ruleReg;
	ruleObj.message += `${isEqual(min, max) ? min : min + '-' + max}个字符`;
	return ruleObj;
};

export const baseRuleReg = getRule(1, 20, RuleTypes.BASE);
export const baseSlashRuleReg = getRule(1, 50, RuleTypes.BASE, RuleTypes.Slash);
export const zhRuleReg = getRule(1, 20, RuleTypes.zh, RuleTypes.BASE);
export const numberReg = getRule(1, 20, RuleTypes.Number);
export const getValid = (...props: any[]) => {
	/**
	 * 方法说明
	 * @param {one: Object} 参数名 rule
	 * @param {two: string|undefined} 参数名 value
	 * @param {three: Function} 参数名 callback
	 * @param {six: boolean} 参数名 isRequire
	 * @param {regObj: {reg: RegExp,message: string}} 参数名 regObj
	 */
	const [, value, callback, , , isRequire, regObj, otherReg] = props;
	if (isEmpty(value) && isRequire) {
		callback(new Error('输入不能为空'));
	} else if (otherReg) {
		if (!otherReg.reg.test(value)) {
			callback(new Error(otherReg.message));
		}
		if (!regObj.reg.test(value)) {
			callback(new Error(regObj.message));
		}
		callback();
	} else if (!regObj.reg.test(value)) {
		callback(new Error(regObj.message));
	} else {
		callback();
	}
};

export const baseValid = (...props: any[]) => getValid(...props, true, baseRuleReg);
export const zhValid = (...props: any[]) => getValid(...props, true, zhRuleReg);
const captchaRegObj = getRule(4, 4, RuleTypes.BASE);
export const captchaValid = (...props: any[]) => getValid(...props, true, captchaRegObj);
const passwordRegObj = getRule(0, 12, RuleTypes.BASE);
export const passwordValid = (...props: any[]) => getValid(...props, false, passwordRegObj);
export const numberValid = (...props: any[]) => getValid(...props, true, numberReg);
export const baseSlashValid = (...props: any[]) =>
	getValid(...props, true, baseSlashRuleReg, {
		reg: /^\//,
		message: '首个字符必须是/'
	});
