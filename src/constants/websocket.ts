export enum WebsocketEventType {
	MESSAGE = 'message',
	PUSH_DATA = 'PUSH_DATA'
}
