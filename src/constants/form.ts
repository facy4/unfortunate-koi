interface RuleProps {
	required?: boolean;
	message?: string;
	trigger?: string;
	min?: number;
	max?: number;
	validator?: any;
}
export interface FormRuleProps {
	[key: string]: Array<RuleProps> | RuleProps;
}
export enum FormItemType {
	INPUT = 'INPUT',
	PASSWORD = 'PASSWORD',
	SELECT = 'SELECT',
	TEXTAREA = 'TEXTAREA'
}
export interface ColumnConfigs {
	[x: string]: string | unknown;
	prop: string;
	label: string;
	render?: (data: {[key: string]: any}) => JSX.Element;
	filter?: (data: string) => string;
}

export interface FormConfig extends ColumnConfigs {
	type: Required<FormItemType>;
	options?: Array<{label: string; value: string}>;
	editDisabled?: boolean;
	addHidden?: boolean;
	rules?: FormRuleProps[];
}
export enum BooleanType {
	False = '0',
	True = '1'
}
export const BooleanStr: {[key: string]: string} = {
	[BooleanType.False]: '否',
	[BooleanType.True]: '是'
};
export const BooleanOptions = Object.values(BooleanType).map(item => ({
	label: BooleanStr[item],
	value: item
}));
