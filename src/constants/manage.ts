export interface UserFormConfig {
	id: number;
	username: string;
	brief: string;
	role: string;
	favour: string;
	gender: string;
	createdAt: Date;
}
export interface RouteFormConfig {
	id: number;
	unique_id: string;
	parent_id: string;
	path: string;
	name: string;
	need_auth: string;
	allow_roles: string;
	need_render: string;
	created_at: Date;
	children?: RouteFormConfig[];
}
export interface ApiFormConfig {
	id: number;
	unique_id: string;
	path: number;
	brief: string;
	need_dencrypt: string;
	need_encrypt: number;
	need_auth: string;
	allow_roles?: string;
	created_at: Date;
}

export enum GenderType {
	Man = '0',
	Woman = '1'
}
export const GenderStr: {[key: string]: string} = {
	[GenderType.Man]: '男',
	[GenderType.Woman]: '女'
};
export const GenderOptions = Object.values(GenderType).map(item => ({
	label: GenderStr[item],
	value: item
}));
export enum ManagerType {
	NormalUser = 'N',
	Manager = 'M',
	SuperManager = 'SM'
}
export const ManagerStr: {[key: string]: string} = {
	[ManagerType.NormalUser]: '普通用户',
	[ManagerType.Manager]: '管理员',
	[ManagerType.SuperManager]: '高级管理员'
};
export const ManagerOptions = Object.values(ManagerType).map(item => ({
	label: ManagerStr[item],
	value: item
}));
