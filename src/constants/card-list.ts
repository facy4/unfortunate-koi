export interface CardDetail {
	id: string;
	date: string;
	title: string;
	subTitle?: string;
	brief?: string;
}
export type CardListConfig = CardDetail[];
