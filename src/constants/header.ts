import {iconGitee, iconWeChart, iconEmail, iconUser, iconSearch, iconLogo, iconClose, iconHome} from '@/assets/header';
export interface NavProps {
	path: string;
	id: number;
	color?: string;
	img: string;
}
export enum Icons {
	GITEE = 'GITEE',
	EMAIL = 'EMAIL',
	WECHART = 'WECHART',
	MENU = 'MENU',
	USER = 'USER',
	SEARCH = 'SEARCH',
	LOGO = 'LOGO',
	CLOSE = 'CLOSE',
	HOME = 'HOME'
}
export const IconsMap: {
	[key: string]: {img: string; text: string; id: string};
} = {
	[Icons.GITEE]: {
		img: iconGitee,
		text: 'GIT',
		id: 'gitee'
	},
	[Icons.EMAIL]: {
		img: iconEmail,
		text: '邮箱',
		id: 'email'
	},
	[Icons.WECHART]: {
		img: iconWeChart,
		text: '微信',
		id: 'wechart'
	},
	[Icons.USER]: {
		img: iconUser,
		text: '用户',
		id: 'user'
	},
	[Icons.SEARCH]: {
		img: iconSearch,
		text: '搜索',
		id: 'search'
	},
	[Icons.LOGO]: {
		img: iconLogo,
		text: '登录',
		id: 'search'
	},
	[Icons.HOME]: {
		img: iconHome,
		text: '主页',
		id: 'home'
	},
	[Icons.CLOSE]: {
		img: iconClose,
		text: '关闭',
		id: 'close'
	}
};
