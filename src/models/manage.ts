import {ApiFormConfig, RouteFormConfig, UserFormConfig} from '@/constants';
import {fetchPost, fetchGet} from '@/utils/fetch';

export const getAllRoutes = async (params: {offset: number; limit: number}) => {
	const {data} = await fetchGet<{list: RouteFormConfig[]}>('/route/getAllRoutes', params);
	return data;
};
export const addNewRoute = async (params: Omit<RouteFormConfig, 'children ' | 'created_at' | 'id'>) =>
	await fetchPost('/route/addNewRoute', params);
export const updateRoute = async (params: Omit<RouteFormConfig, 'children ' | 'created_at' | 'id'>) =>
	await fetchPost('/route/updateRoute', params);
export const deleteRoute = async (params: {unique_id: string}) => await fetchPost('/route/deleteRoute', params);

export const getAllApis = async (params: {offset: number; limit: number}) => {
	const {data} = await fetchGet<{list: ApiFormConfig[]}>('/api/getAllApis', params);
	return data;
};
export const addNewApi = async (params: Omit<ApiFormConfig, 'created_at'>) => await fetchPost('/api/addNewApi', params);
export const updateApi = async (params: Omit<ApiFormConfig, 'created_at'>) => await fetchPost('/api/updateApi', params);
export const deleteApi = async (params: {unique_id: string}) => await fetchPost('/api/deleteApi', params);

export const getAllUsers = async (params: {offset: number; limit: number}) => {
	const {data} = await fetchGet<{list: UserFormConfig[]}>('/user/getAllUsers', params);
	return data;
};
export const addUser = async (params: Omit<UserFormConfig, 'createdAt'>) => await fetchPost('/user/addUser', params);
export const updateUser = async (params: Omit<UserFormConfig, 'createdAt'>) =>
	await fetchPost('/user/updateUser', params);
export const deleteUser = async (params: {id: number}) => await fetchPost('/user/deleteUser', params);
