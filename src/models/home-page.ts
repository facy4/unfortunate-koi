import {fetchPost, fetchGet} from '@/utils/fetch';

export const toLogin = async (params: {username: string; password: string; captcha: string}) => {
	return await fetchPost<{
		token: string;
		username: string;
		userid: string;
	}>('/user/login', params);
};
export const getAphorism = async () => {
	const {data} = await fetchGet<{content: string; title: string}>('/aphorism/getAphorism');
	return data;
};
export const getWeather = async (params: {
	unescape: number;
	version: string;
	appid: string;
	appsecret: string;
	cityid: string;
}) => await fetchGet<any>('https://v0.yiketianqi.com/api', params, {cancelProxy: true});
