import {AccumulationType, CardListConfig, IAccumulation} from '@/constants';
import {fetchPost, fetchGet} from '@/utils/fetch';

export const insertAccumulation = async (params: {
	title: string;
	content: string;
	type: keyof typeof AccumulationType;
	author: string;
	brief: string;
	like_num: number;
}) => {
	return await fetchPost('/accumulation/insertAccumulation', params);
};

export const getAccumulationList = async (params: {
	offset: number;
	limit: number;
	type: keyof typeof AccumulationType;
}) => {
	const {data} = await fetchGet<{list: CardListConfig; total: number}>('/accumulation/getAccumulationList', params);
	return data;
};
export const getAccumulation = async (params: {id: number; type: keyof typeof AccumulationType}) => {
	const {data} = await fetchGet<{articleDetail: IAccumulation}>('/accumulation/getAccumulation', params, {
		slient: true
	});
	return data;
};
export const changeAccumulationLikeState = async (params: {id: number; likeFlag: boolean; userId: string}) => {
	const {data} = await fetchPost('/accumulation/changeAccumulationLikeState', params, {slient: true});
	return data;
};
