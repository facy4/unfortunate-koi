import {defineComponent, PropType} from 'vue';
const sizeMap = {
	sm: 12,
	md: 16,
	lg: 24
};
export default defineComponent({
	name: 'CustomText',
	props: {
		color: {
			type: String,
			default: '#000'
		},
		size: {
			type: String as PropType<'sm' | 'md' | 'lg'>,
			required: false
		},
		style: {
			type: Object,
			required: false
		}
	},
	setup(props, {attrs, slots}) {
		const otherStyle = props?.style || {};
		return () => (
			<span {...attrs} style={{...otherStyle, color: props.color, fontSize: `${sizeMap[props.size || 'sm']}px`}}>
				{slots.default && slots.default()}
			</span>
		);
	}
});
