import * as models from '@/models';
import {computed, defineComponent, onMounted, PropType, reactive, ref} from 'vue';
import {FormConfig, FormItemType, FormRuleProps, captchaValid, zhValid, baseValid} from '@/constants';
import NormalForm from '@/components/NormalForm';
import {useStore} from 'vuex';
import {ElNotification} from 'element-plus';
import {useRouter} from 'vue-router';
import {getMenu} from '@/router';
import {noop} from '.pnpm/@vueuse+shared@9.3.1_vue@3.2.41/node_modules/@vueuse/shared';
interface LoginState {
	loginSwitch: boolean;
	captchaSrc: string;
	safeQuestions: {value: string; label: string}[];
}

const getRules = (isLogin = false) => {
	const rules: FormRuleProps = {
		username: [
			{
				validator: zhValid,
				trigger: 'blur'
			}
		],
		password: [
			{
				validator: baseValid,
				trigger: 'blur'
			}
		],
		captcha: [
			{
				validator: captchaValid,
				trigger: 'blur'
			}
		]
	};
	if (!isLogin) {
		rules.safeQuestion = [
			{
				required: true,
				message: '请输入和选择选择安全问题',
				trigger: 'blur'
			}
		];
		rules.safeAnswer = [
			{
				required: true,
				message: '请输入安全密码',
				trigger: 'blur'
			}
		];
	}
	return rules;
};
export default defineComponent({
	name: 'Login',
	props: {
		onClose: {
			type: Function as PropType<VoidFunction>,
			default: noop
		}
	},
	components: {[NormalForm.name]: NormalForm},
	setup(props) {
		const viewState = reactive<LoginState>({
			loginSwitch: false,
			captchaSrc: '',
			safeQuestions: [
				{
					value: 'primarySchool',
					label: '你的小学叫什么'
				}
			]
		});
		const formRef = ref();
		const store = useStore();
		const router = useRouter();
		const getFormConfigs: (isRegister?: boolean) => FormConfig[] = isRegister => {
			const formConfig = [
				{
					prop: 'username',
					type: FormItemType.INPUT,
					label: '用户名',
					editDisabled: true
				},
				{
					prop: 'password',
					type: FormItemType.PASSWORD,
					label: '密码'
				},
				{
					prop: 'captcha',
					type: FormItemType.INPUT,
					label: '验证码',
					render: (data: any) => {
						return (
							<div class="captcha-content">
								<el-input maxlength="4" class="captcha-input" v-model={data.captcha}></el-input>
								<img class="captcha-img" onClick={() => getCaptcha()} src={viewState.captchaSrc} />
							</div>
						);
					}
				}
			];
			if (isRegister) {
				formConfig.push({
					prop: 'username',
					type: FormItemType.INPUT,
					label: '用户名',
					editDisabled: true
				});
			}
			return formConfig;
		};
		const userinfo = computed(() => store.state.userinfo);
		const getCaptcha = async () => {
			viewState.captchaSrc = `/api/user/captcha?rodomNum=${Math.random()}`;
		};
		onMounted(() => {
			getCaptcha();
		});
		const onSubmit = async () => {
			await formRef.value.validate();
			if (!viewState.loginSwitch) {
				const res = await models.toLogin(formRef.value.getformData());
				store.commit('setUserinfo', {...res.data, loginState: true});
				ElNotification({
					title: 'Success',
					message: '登录成功',
					type: 'success'
				});
			}
			props.onClose && props.onClose();
		};
		const exitLogin = () => {
			store.commit('resetState');
			router.replace('/');
			getMenu();
		};
		return () => {
			return userinfo.value.loginState ? (
				<section>
					<div>userid:{userinfo.value.userid}</div>
					<div>username:{userinfo.value.username}</div>
					<div class="text-center mt10">
						<el-button onClick={exitLogin}>退出登录</el-button>
					</div>
				</section>
			) : (
				<section class="login-container">
					<div
						class={[
							'login-oprate',
							'animate__animated ',
							!viewState.loginSwitch ? 'animate__backInRight' : 'animate__backInLeft'
						].join(' ')}
					>
						<el-button
							onClick={() => {
								onSubmit();
							}}
						>
							{!viewState.loginSwitch ? '登录' : '注册'}
						</el-button>
					</div>
					<NormalForm
						ref={formRef}
						class={[
							'login-form',
							'animate__animated ',
							!viewState.loginSwitch ? 'animate__backInLeft' : 'animate__backInRight'
						].join(' ')}
						label-width="60px"
						formConfigs={getFormConfigs(viewState.loginSwitch)}
						rules={getRules()}
						v-slots={{
							footer: () => (
								<section class="text-center w240">
									{/* <el-button onClick={() => {
                viewState.loginSwitch = !viewState.loginSwitch
              }}>{!viewState.loginSwitch ? '去注册' : '去登录'}</el-button> */}
								</section>
							)
						}}
					></NormalForm>
				</section>
			);
		};
	}
});
