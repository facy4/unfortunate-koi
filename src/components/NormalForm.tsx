import {FormConfig, FormItemType, FormRuleProps} from '@/constants';
import {NormalObject} from '@/constants/global';
import {defineComponent, PropType, reactive, ref, watch} from 'vue';
export default defineComponent({
	name: 'NormalForm',
	inheritAttrs: false,
	props: {
		initData: {
			type: Object,
			default: () => ({})
		},
		formConfigs: {
			type: Array as PropType<FormConfig[]>,
			required: true
		},
		rules: {
			type: Object as PropType<FormRuleProps>,
			required: true
		},
		showFormOprate: {
			type: Boolean,
			default: false
		},
		onCancel: {
			type: Function as PropType<VoidFunction>,
			required: false
		},
		onSubmit: {
			type: Function as PropType<(formData: NormalObject) => Promise<void>>,
			required: false
		},
		resetTable: {
			type: Function as PropType<VoidFunction>,
			required: false
		}
	},
	setup(props, ctx) {
		const formRef = ref();
		const initFormData = () =>
			props.formConfigs.reduce((pre: any, next: FormConfig) => {
				if (props.initData.id && !pre.id) {
					pre.id = props.initData.id;
				}
				pre[next.prop] = props.initData[next.prop] || '';
				if (next.type === FormItemType.SELECT) {
					pre[next.prop] = props.initData[next.prop] || (next.options && next.options[0].value);
				}
				return pre;
			}, {});
		const viewState = reactive<{formData: NormalObject}>({
			formData: initFormData()
		});
		ctx.expose({
			getformData: () => viewState.formData,
			validate: () => formRef.value.validate()
		});
		const onSubmitCallBack = async () => {
			if (!formRef.value) return;
			try {
				await formRef.value.validate();
				props.onSubmit && props.onSubmit(viewState.formData);
			} catch (err) {
				console.log(err);
			}
		};
		watch(
			() => [props.initData],
			([initData]: any) => {
				viewState.formData = initData;
			}
		);
		const renderOprate = () => {
			return (
				<div class="text-center">
					<el-button type="primary" size="large" class="w120" onClick={() => onSubmitCallBack()}>
						确定
					</el-button>
					<el-button size="large" class="w120" onClick={props.onCancel}>
						取消
					</el-button>
				</div>
			);
		};
		const renderForm = () => {
			const renderFormItemValue = (item: FormConfig) => {
				if (item.render) {
					return item.render(viewState.formData);
				}
				switch (item.type) {
					case FormItemType.INPUT:
						return <el-input v-model={viewState.formData[item.prop]}></el-input>;
					case FormItemType.PASSWORD:
						return <el-input type="password" show-password v-model={viewState.formData[item.prop]}></el-input>;
					case FormItemType.SELECT:
						return (
							<el-select v-model={viewState.formData[item.prop]}>
								{item.options?.map(item => {
									return <el-option key={item.value} value={item.value} label={item.label}></el-option>;
								})}
							</el-select>
						);
					case FormItemType.TEXTAREA:
						return <el-input v-model={viewState.formData[item.prop]} type="textarea"></el-input>;
				}
			};
			return props.formConfigs.map(item => {
				return (
					<el-form-item key={item.prop} prop={item.prop} label={item.label}>
						{renderFormItemValue(item)}
					</el-form-item>
				);
			});
		};
		return () => {
			return (
				<>
					<el-form ref={formRef} model={viewState.formData} rules={props.rules} {...ctx.attrs}>
						{renderForm()}
						{ctx.slots?.footer && ctx.slots.footer()}
					</el-form>
					{props.showFormOprate ? renderOprate() : null}
				</>
			);
		};
	}
});
