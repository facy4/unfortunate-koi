import {defineComponent} from 'vue';
export default defineComponent({
	name: 'Footer',
	setup() {
		return () => {
			return (
				<section style={{height: '100px'}} class="footer flex-layout align-center inherit-w-h pd10">
					<div class="flex-one"></div>
					<div class="flex-one flex-layout column content-center inherit-w-h ">
						{/* <div class="flex-layout flex-one content-center">
							<div style={{width: '200px', backgroundColor: 'green'}} class="min-program text-center">
								小程序icon
							</div>
						</div> */}
						<div class="text-center">
							ICP备案/许可证号：
							<a href="https://beian.miit.gov.cn/" target="_blank">
								黔ICP备2022003861号-1
							</a>
							<a
								class="text-center"
								target="_blank"
								href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=52010302002810"
							>
								贵公网安备 52010302002810号
							</a>
						</div>
					</div>
					<div class="flex-one"></div>
				</section>
			);
		};
	}
});
