import {defineComponent, PropType} from 'vue';
import NormalForm from '@/components/NormalForm';
import CustomText from './CustomText';
import {CardDetail, CardListConfig} from '@/constants/card-list';
import {useRouter} from 'vue-router';

const CardContainer = defineComponent({
	name: 'CardContainer',
	props: {
		detail: {
			type: Object as PropType<CardDetail>,
			require: true
		}
	},
	setup(props, {slots}) {
		const router = useRouter();
		const onClick = ({detail}: typeof props) => {
			router.push(`/accumulation/detail/${detail?.id}`);
		};
		return () => (
			<section class="card-container-wrapper">
				<div class="card-container">
					<div class="card-date">{props.detail?.date}</div>
					<div class="card-title text-center">{props.detail?.title}</div>
					<div class="card-sub-title text-center">{props.detail?.subTitle}</div>
					<div class="card-brief">{slots.brief ? slots.brief() : props.detail?.brief}</div>
					<div class="card-bottom flex-layout between">
						<div class="card-bottom-left">left</div>
						<div class="card-bottom-right" onClick={() => onClick(props)}>
							详情
						</div>
					</div>
				</div>
			</section>
		);
	}
});
export default defineComponent({
	name: 'CardList',
	props: {
		title: {
			type: String,
			required: false
		},
		listConfigs: {
			type: Array as PropType<CardListConfig>,
			default: () => []
		},
		total: {
			type: Number
		}
	},
	components: {[NormalForm.name]: NormalForm},
	setup(props, {slots}) {
		const renderTitle = () => {
			return slots.title ? (
				slots.title()
			) : (
				<el-row class="mt30" style={{padding: '16px'}}>
					<el-col span={24}>
						{props.title && <CustomText size="lg">{props.title}</CustomText>}共{props.total}篇文章
					</el-col>
				</el-row>
			);
		};
		const renderContent = () => {
			return (
				<section class="card-list-content">
					{props.listConfigs.map(detail => {
						return <CardContainer detail={detail} />;
					})}
				</section>
			);
		};
		return () => (
			<section class="card-list">
				{renderTitle()}
				{renderContent()}
			</section>
		);
	}
});
