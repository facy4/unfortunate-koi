import {computed, defineComponent, inject, watch} from 'vue';

export default defineComponent({
	name: 'CustomDialog',

	setup(props, {slots, attrs}) {
		let firstRender = true;
		const customDialogState = inject<{
			visible: boolean;
		}>('customDialogState') || {visible: false};

		watch(
			() => customDialogState.visible,
			() => {
				firstRender = false;
			}
		);
		const currentClass = computed(() => {
			const currentClassClassStr = customDialogState.visible ? 'custom-dialog ' : 'custom-dialog hide ';
			return firstRender ? 'custom-dialog entire-hide' : currentClassClassStr;
		});
		return () => (
			<section {...attrs} class={currentClass.value}>
				{slots.default && slots.default()}
			</section>
		);
	}
});
