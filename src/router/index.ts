import * as models from '@/models';
import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
import {defineAsyncComponent} from 'vue';
import Home from '../views/Home';
import Manage from '../views/manage/Index.vue';
import Users from '../views/manage/Users.vue';
import Routes from '../views/manage/Routes.vue';
import Api from '../views/manage/Api.vue';
import Accumulation from '../views/accumulation/Index.vue';
import AccumulationDetail from '../views/accumulation/AccumulationDetail.vue';
import AddArticle from '../views/accumulation/AddArticle.vue';
import ArticleList from '../views/accumulation/ArticleList.vue';
import Check from '../views/webrtc/Check.vue';
import WebRtcP2P from '../views/webrtc/WebRtcP2P.vue';
import NotFound from '@/views/NotFound';
import ReactComponent from '../views/react';
import {RouteFormConfig} from '@/constants';
import store from '@/store';
const WebRTC = defineAsyncComponent(() => import(/* webpackChunkName: "webrtc" */ '../views/webrtc/WebRTC.vue'));
let routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/manage',
		name: 'manage',
		component: Manage,
		children: [
			{
				path: 'users',
				name: 'manage_users',
				component: Users
			},
			{
				path: 'routes',
				name: 'manage_route',
				component: Routes
			},
			{
				path: 'rapis',
				name: 'manage_rapis',
				component: Api
			}
		]
	},
	{
		path: '/react',
		name: 'react',
		component: ReactComponent
	},
	{
		path: '/webrtc',
		name: 'webrtc',
		component: WebRTC,
		children: [
			{
				path: 'p2p',
				name: 'webrtc_p2p',
				component: WebRtcP2P
			},
			{
				path: 'check',
				name: 'webrtc_check',
				component: Check
			}
		]
	},
	{
		path: '/addarticle',
		name: 'addArticle',
		component: AddArticle
	},
	{
		path: '/accumulation',
		name: 'accumulation',
		component: Accumulation,
		redirect: '/accumulation/article-list',
		children: [
			{
				path: 'detail/:id',
				name: 'accumulation_detail',
				component: AccumulationDetail
			},
			{
				path: 'article-list',
				name: 'accumulation_list',
				component: ArticleList
			}
		]
	},
	{
		path: '/test',
		name: 'Test',
		component: () => import(/* webpackChunkName: "about" */ '../views/Test.vue')
	},
	{path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound}
];
const handleMenuList = (MenuList: RouteFormConfig[], parentId: number) => {
	const menuList: RouteFormConfig[] = [];
	MenuList.forEach(item => {
		if (!Number(item.need_render)) return;
		if (item.parent_id === String(parentId)) {
			menuList.push(item);
			item.children = handleMenuList(MenuList, item.id);
		}
	});
	return menuList;
};
const handleMenuRoles = (
	roleMap: {
		[key: string]: {
			need_auth: string;
			allow_roles: string;
		};
	},
	routeList: RouteRecordRaw[]
) => {
	return routeList.map(item => {
		if (item.children && item.children?.length > 0) {
			handleMenuRoles(roleMap, item.children);
		}
		if (!item.meta) {
			const currentMeta = roleMap[item.name as string];
			if (currentMeta) {
				item['meta'] = currentMeta;
			}
		}
		return item;
	});
};
export const getMenu = async () => {
	const {list} = await models.getAllRoutes({offset: 0, limit: 50});
	const roleMap = list.reduce(
		(perv, next) => {
			if (!Number(next.need_render)) return perv;
			perv[next.unique_id] = {
				need_auth: next.need_auth,
				allow_roles: next.allow_roles
			};
			return perv;
		},
		{} as {
			[key: string]: {
				need_auth: string;
				allow_roles: string;
			};
		}
	);
	const menuList = handleMenuList(list, 0);
	routes = handleMenuRoles(roleMap, routes);
	store.commit('setMenuList', menuList);
	return routes;
};
const router = async () =>
	createRouter({
		history: createWebHistory(process.env.BASE_URL),
		routes: await getMenu()
	});
export default router();
