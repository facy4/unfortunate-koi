import {RouteFormConfig} from '@/constants';
import {initState} from '@/constants/store';
import {createStore} from 'vuex';
import createPersistedState from 'vuex-persistedstate';
export default createStore({
	plugins: [
		createPersistedState({
			// 默认存储在localStorage 现改为sessionStorage
			storage: window.localStorage,
			// 本地存储数据的键名
			key: 'vuex'
			// 指定需要存储的模块，如果是模块下具体的数据需要加上模块名称，如user.token
		})
	],
	state: initState,
	getters: {
		getUserInfo: (state: typeof initState) => {
			return state.userinfo;
		}
	},
	mutations: {
		resetState(state: any) {
			for (const key in state) {
				if (Object.prototype.hasOwnProperty.call(state, key) && key !== 'menuList') {
					state[key] = (initState as typeof state)[key];
				}
			}
		},
		setIv(state: {iv: string}, iv: string) {
			state.iv = iv;
		},
		setShowMenu(state: typeof initState, showState: boolean) {
			state.showMenu = showState;
		},
		setUserinfo(
			state: typeof initState,
			userinfo: {
				token: string;
				username: string;
				userid: string;
				loginState: boolean;
			}
		) {
			state.userinfo = userinfo;
		},
		setneedDEncryptApiList(state: typeof initState, needDEncryptApiList: string[]) {
			state.needDEncryptApiList = needDEncryptApiList;
		},
		setneedEncryptApiList(state: typeof initState, needEncryptApiList: string[]) {
			state.needEncryptApiList = needEncryptApiList;
		},
		setNeedAuthhList(state: typeof initState, needAuthApiList: string[]) {
			state.needAuthApiList = needAuthApiList;
		},
		setMenuList(state: typeof initState, menuList: RouteFormConfig[]) {
			state.menuList = menuList;
		},
		setIpConfigInfo(
			state: typeof initState,
			ipConfigInfo: {
				city: string;
				ip: string;
				addr: string;
			}
		) {
			state.ipConfigInfo = ipConfigInfo;
		}
	},
	actions: {},
	modules: {}
});
