export {default as iconGitee} from './gitee.svg';
export {default as iconWeChart} from './wechart.svg';
export {default as iconEmail} from './email.svg';
export {default as iconSearch} from './search.svg';
export {default as iconUser} from './user.svg';
export {default as iconLogo} from './logo.svg';
export {default as iconClose} from './close.svg';
export {default as iconHome} from './home.svg';
