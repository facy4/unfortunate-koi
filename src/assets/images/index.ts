export {default as iconBeiAn} from './beian.png';
export {default as iconRefresh} from './refresh.svg';
export {default as iconNormalArrow} from './normal-arrow.svg';
export {default as iconFinger} from './finger.svg';
export {default as iconLike} from './like.svg';
export {default as iconLikeActive} from './like-active.svg';
export {default as iconQuestion} from './question.svg';
