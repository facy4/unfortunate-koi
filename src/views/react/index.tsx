import {defineComponent} from 'vue';
export default defineComponent({
	name: 'React',
	setup() {
		return () => {
			return (
				<section class="inherit-w-h ">
					<a href="http://47.106.136.231/test" target="_blank">
						https不支持iframe http 网站 点击跳转
					</a>
					<iframe
						style={{minHeight: '800px', width: '100%', overflow: 'hidden'}}
						src={`http://${process.env.NODE_ENV === 'development' ? 'localhost:3000' : '47.106.136.231'}/test`}
						frameborder="0"
					></iframe>
				</section>
			);
		};
	}
});
