import {defineComponent} from 'vue';
import NormalForm from '@/components/NormalForm';
import '@/assets/scss/home.scss';
import CardList from '@/components/CardList';
import {IconsMap, Icons} from '@/constants';
import {lazyimg, useIntersectionObserver} from '@/hooks/useIntersectionObserver';
import {ref} from 'vue';

export default defineComponent({
	name: 'Home',
	components: {
		[NormalForm.name]: NormalForm,
		[CardList.name]: CardList
	},
	directives: {
		lazyimg
	},
	setup() {
		const previewList = [
			{
				key: '1',
				img: 'https://s1.imagehub.cc/images/2023/04/01/10a84e01401cb8be44cb3317360fbc56.webp',
				label: 'node中文网',
				href: 'https://nodejs.cn/api/'
			},
			{
				key: '2',
				img: 'https://s1.imagehub.cc/images/2023/04/01/349a78e78528babde62c6edf9ab49a97.jpeg',
				label: 'webrtc好的文章推荐',
				href: 'https://zhuanlan.zhihu.com/p/468792680'
			},
			{
				key: '3',
				img: 'https://s1.imagehub.cc/images/2023/04/01/7c2a4fec476fa8ff3cdf7f4d5aa4b54a.jpeg',
				label: '免费好用的图床网站',
				href: 'https://www.imagehub.cc/'
			}
		];
		const imgRef = ref<HTMLImageElement | null>(null);
		const {stop} = useIntersectionObserver(imgRef, evt => {
			if (imgRef.value && evt[0].isIntersecting) {
				console.log((imgRef.value as any).alt);
				imgRef.value.src = (imgRef.value as any).alt;
				stop();
			}
		});
		return () => {
			return (
				<section class="home">
					<div class="home-content">
						<div class="home-main">
							<article>
								<h1 class="home-font1">this is my blog</h1>
								<p class="home-font2">分享生活记录生活</p>
								<h2 class="home-font3">向前走一切都会变好的</h2>
							</article>
							<div class="mb30 text-right">
								<a href="https://gitee.com/facy4/unfortunate-koi" target="_blank">
									gitee前端
									<img
										class="icon-normal ml30 "
										style={{verticalAlign: 'sub'}}
										src={IconsMap[Icons.GITEE].img}
										alt=""
									/>
								</a>
							</div>
							<div class="text-right">
								<a href="https://gitee.com/facy4/unfortunate-koi-server" target="_blank">
									gitee后端
									<img class="icon-normal ml30" style={{verticalAlign: 'sub'}} src={IconsMap[Icons.GITEE].img} alt="" />
								</a>
							</div>
							<div class="preview-list">
								{previewList.map(item => (
									<div key={item.key} class="preview-list-box">
										<a href={item.href} target="_blank">
											<img v-lazyimg src="" alt={item.img} />
										</a>
										<div class="text-center">{item.label}</div>
									</div>
								))}
							</div>
						</div>
						<div class="home-sidebar">
							<div>
								<img
									v-lazyimg
									class="first-img"
									src=""
									alt="https://s1.imagehub.cc/images/2023/04/01/24afa3f267b9acc4dff399d4cbddb7cb.png"
								/>
								<div class="first-text">活下去总有走运的时候</div>
							</div>
						</div>
					</div>
				</section>
			);
		};
	}
});
