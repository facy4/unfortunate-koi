import {defineComponent} from 'vue';
import '@/assets/scss/not-found.scss';
export default defineComponent({
	name: 'NotFound',
	setup() {
		const notFoundArr = new Array(40).fill(4).concat(new Array(40).fill(0));
		return () => (
			<main class="not-found-container">
				{notFoundArr.map(item => (
					<span class="particle">{item}</span>
				))}
				<article class="not-found-content">
					<p>迷路了么，</p>
					<p>
						你来到了 <strong>404</strong> 星球。
					</p>
					<p>
						<button>
							<router-link to="/">点我回家。</router-link>
						</button>
					</p>
				</article>
			</main>
		);
	}
});
