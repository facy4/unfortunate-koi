import {ElNotification} from 'element-plus';
import {ResponseProps} from '@/constants/global';
export const debounce = (callback: any, wait: number) => {
	let timeout: null | NodeJS.Timeout = null;
	return (...args: any[]) => {
		timeout && clearTimeout(timeout);
		timeout = setTimeout(() => {
			callback(...args);
		}, wait);
	};
};

export function throttle(callback: any, wait: number) {
	let timeout: null | NodeJS.Timeout = null;
	return (...args: any[]) => {
		if (!timeout) {
			timeout = setTimeout(() => {
				callback(...args);
				timeout = null;
			}, wait);
		}
	};
}

export const handleSuccess = ({msg}: ResponseProps) => {
	ElNotification({
		title: 'Success',
		message: msg,
		type: 'success'
	});
};

export const smallHumpToUnderline: <T>(originObj: T) => T = (originObj: any) => {
	return Object.keys(originObj).reduce((pre, item: string) => {
		let underlineName = item;
		const fieds = item.match(/([A-Z][a-z]+)/g);
		Array.isArray(fieds) &&
			fieds.forEach(fiedItem => {
				underlineName = underlineName.replace(fiedItem, `_${fiedItem.toLocaleLowerCase()}`);
			});
		pre[underlineName] = originObj[item];
		return pre;
	}, {} as any);
};

export const sleep: (time: number) => Promise<void> = time => {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve();
		}, time);
	});
};
