import {config} from '@/constants/config';
import store from '@/store';
import {AnyObject} from '@/constants/global';
import querystring from 'querystring';
import Loading from '@/components/Loading';
import {encryptInstance} from './encrypt';
import {ElNotification} from 'element-plus';
import {sleep} from './tools';
import {nanoid} from 'nanoid';
import {useMask} from '@/hooks/useMask';
let resendTimes = 0;
const timeOutTime = 6000;
const overTimeFn = () =>
	new Promise((resolve, reject) => {
		setTimeout(() => {
			reject('请求超时了~');
		}, Number(process.env.VUE_APP_TIMEOUT) || timeOutTime);
	});
interface ResponseData<T> {
	data: T;
	msg: string;
	code: number;
}
interface MenuOptions {
	cancelProxy?: boolean;
	slient?: boolean;
}
interface SendOptions {
	headers: Headers;
	method: string;
	body?: string;
}
interface RequestOptions {
	headerOptions: HeadersInit;
	method: 'POST' | 'GET';
	body?: string;
	query?: string;
	cancelProxy?: boolean;
	signal: AbortSignal;
}
interface RequestData {
	<T>(url: string, requestOptions: RequestOptions): Promise<ResponseData<T>>;
}
interface FetchGetData {
	<T>(url: string, params?: AnyObject, options?: MenuOptions): Promise<ResponseData<T>>;
}
interface FetchPostData {
	<T>(url: string, params?: AnyObject, options?: MenuOptions): Promise<ResponseData<T>>;
}
export const requestData: RequestData = async (url, requestOptions) => {
	let sendUrl = (requestOptions.cancelProxy ? '' : config.baseURL) + url;
	const headers = new Headers({
		Timestamp: String(+new Date()),
		RequestId: nanoid(),
		...requestOptions.headerOptions
	});

	const sendOptions: SendOptions = {
		headers,
		method: requestOptions.method
	};

	if (requestOptions.method === 'POST' && requestOptions.body) {
		sendOptions.body = requestOptions.body;
	}

	if (requestOptions.method === 'GET' && requestOptions.query) {
		sendUrl = sendUrl + '?' + requestOptions.query;
	}
	const request = new Request(sendUrl, sendOptions);
	const fetchResponse = await fetch(request, {signal: requestOptions.signal});

	if (!fetchResponse.ok) {
		store.commit('resetState');
		ElNotification({
			title: 'Error',
			message: '请求错误，正在重试',
			type: 'error'
		});
		if (resendTimes !== 3) {
			console.error('----resend--fetchResponse.ok', request);
			await sleep(2000);
			requestData(url, requestOptions);
			resendTimes += 1;
		} else {
			ElNotification({
				title: 'Error',
				message: '重新请求次数超过3次，即将跳转首页',
				type: 'error'
			});
			await sleep(2000);
			window.location.href = '/';
		}
		return Promise.reject(fetchResponse.statusText);
	}
	const fetchResponseJson = await fetchResponse.json();
	if (fetchResponseJson.code && fetchResponseJson.code === 403) {
		store.commit('resetState');
		await updateEncryptList();
		ElNotification({
			title: fetchResponseJson.code,
			message: fetchResponseJson.msg,
			type: 'error'
		});
		return Promise.reject(fetchResponseJson.msg);
	}
	if (fetchResponseJson.code && !/^2/.test(String(fetchResponseJson.code))) {
		ElNotification({
			title: fetchResponseJson.code,
			message: fetchResponseJson.msg,
			type: 'error'
		});
		return;
	}
	if (!Array.isArray(store.state.needEncryptApiList)) {
		return Promise.reject({
			code: 400,
			message: '解密数组有问题'
		});
	}
	if (store.state.needEncryptApiList.includes(url || '')) {
		try {
			fetchResponseJson.data = JSON.parse(encryptInstance.toDAesString(fetchResponseJson.data));
		} catch (error) {
			console.error('解密失败', error);
			store.commit('resetState');
			return requestData(url, requestOptions);
		}
	}
	return fetchResponseJson;
};
export const fetchGet: FetchGetData = async (url, params, options = {}) => {
	const {showMask, hideMask} = useMask({component: Loading, mountElId: 'loding'});
	if (!store?.state?.iv) {
		await updateEncryptList();
	}
	const controller = new AbortController();
	const signal = controller.signal;
	!options?.slient && showMask();
	const headerOptions: {
		Authentication: string;
		'Content-Type': string;
	} = {
		Authentication: '',
		'Content-Type': 'application/json;charset=utf-8'
	};
	if (store.state.needAuthApiList.includes(url)) {
		if (!store.state.userinfo.token) {
			ElNotification({
				title: '403',
				message: '请先登录哦',
				type: 'error'
			});
			// to login
			hideMask();
			return Promise.reject('未登录');
		}
		headerOptions.Authentication = store.state.userinfo.token;
	}
	resendTimes = 0;
	const requestRes: any = await Promise.race([
		requestData(url, {
			signal,
			headerOptions,
			method: 'GET',
			query: querystring.stringify(params),
			...options
		}),
		overTimeFn()
	]).catch(err => {
		controller.abort();
		ElNotification({
			title: '500',
			message: err,
			type: 'error'
		});
		hideMask();
		return {};
	});

	hideMask();
	return requestRes;
};

const updateEncryptList = async () => {
	const {
		data: {
			iv,
			needDEncryptList: needDEncryptApiList,
			needEncryptList: needEncryptApiList,
			needAuthList: needAuthApiList
		}
	} = await (await fetch('/api/user/secret')).json();
	store.commit('setIv', iv);
	store.commit('setneedEncryptApiList', needEncryptApiList);
	store.commit('setneedDEncryptApiList', needDEncryptApiList);
	store.commit('setNeedAuthhList', needAuthApiList);
	encryptInstance.setIv(iv);
};

export const fetchPost: FetchPostData = async (url, params, options = {}) => {
	const {showMask, hideMask} = useMask({component: Loading, mountElId: 'loding'});
	if (!store?.state?.iv) {
		await updateEncryptList();
	}
	const controller = new AbortController();
	const signal = controller.signal;
	!options?.slient && showMask();
	const sendParams = {
		reqData: store.state.needDEncryptApiList.includes(url || '')
			? encryptInstance.toAesString(JSON.stringify(params) as any)
			: params
	};
	const headerOptions: {
		Authentication: string;
		'Content-Type': string;
	} = {
		Authentication: '',
		'Content-Type': 'application/json;charset=utf-8'
	};
	if (store.state.needAuthApiList.includes(url)) {
		if (!store.state.userinfo.token) {
			ElNotification({
				title: '403',
				message: '请先登录哦',
				type: 'error'
			});
			// to login
			hideMask();
			return Promise.reject('未登录');
		}
		headerOptions.Authentication = store.state.userinfo.token;
	}
	resendTimes = 0;
	const requestRes: any = await Promise.race([
		requestData(url, {
			signal,
			headerOptions,
			method: 'POST',
			body: JSON.stringify(sendParams),
			...options
		}),
		overTimeFn()
	]).catch(err => {
		controller.abort();
		hideMask();
		return Promise.reject(err);
	});

	hideMask();
	return requestRes;
};
