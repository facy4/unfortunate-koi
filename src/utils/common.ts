export const isEmpty = (value: any) => {
	if (value == null) return true;

	if (typeof value === 'boolean') return false;

	if (typeof value === 'number') return !value;

	if (value instanceof Error) return value.message === '';

	switch (Object.prototype.toString.call(value)) {
		// String or Array
		case '[object String]':
		case '[object Array]':
			return !value.length;

		// Map or Set or File
		case '[object File]':
		case '[object Map]':
		case '[object Set]': {
			return !value.size;
		}
		// Plain Object
		case '[object Object]': {
			return !Object.keys(value).length;
		}
	}
	return false;
};

export const isEqual = (valueOne: any, valueTwo: any) => {
	//不是对象,直接返回比较结果
	if (typeof valueOne !== 'object' || typeof valueTwo !== 'object') {
		return valueOne === valueTwo;
	}
	//都是对象,且地址相同,返回true
	if (valueTwo === valueOne) return true;
	//是对象或数组
	const keys1 = Object.keys(valueOne);
	const keys2 = Object.keys(valueTwo);
	//比较keys的个数,若不同,肯定不相等
	if (keys1.length !== keys2.length) return false;
	for (const k of keys1) {
		//递归比较键值对
		const res = isEqual(valueOne[k], valueTwo[k]);
		if (!res) return false;
	}
	return true;
};
