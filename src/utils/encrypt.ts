import {config} from '@/constants/config';
import * as CryptoJS from 'crypto-js';
import store from '@/store';

class Encrypt {
	private iv: string;
	private key: string;
	constructor() {
		this.key = config.encryptKey;
		this.iv = store.state.iv || '';
	}
	toAesString = (cleartext: CryptoJS.lib.WordArray) => {
		try {
			const encrypted = CryptoJS.AES.encrypt(cleartext, CryptoJS.enc.Utf8.parse(this.key), {
				iv: CryptoJS.enc.Utf8.parse(this.iv),
				mode: CryptoJS.mode.CBC,
				padding: CryptoJS.pad.Pkcs7
			});
			//返回的是base64格式的密文
			return encrypted.toString();
		} catch (err) {
			console.log(err);
		}
	};

	setIv = (iv: string) => {
		this.iv = iv;
	};

	toDAesString(ciphertext: string) {
		const decrypted = CryptoJS.AES.decrypt(ciphertext, CryptoJS.enc.Utf8.parse(this.key), {
			iv: CryptoJS.enc.Utf8.parse(this.iv),
			mode: CryptoJS.mode.CBC,
			padding: CryptoJS.pad.Pkcs7
		});
		return decrypted.toString(CryptoJS.enc.Utf8);
	}
}
export const encryptInstance = new Encrypt();
