import {config} from '@/constants/config';
import shortid from 'shortid';
import {ElMessage} from 'element-plus';
const heartBeatTime = 30000;
export class EventEmit {
	eventList: Map<string, () => any>;
	constructor() {
		this.eventList = new Map();
	}
	on(eventName: string, callback: (...args: unknown[]) => void) {
		this.eventList.set(eventName, callback);
		return this;
	}
	off(eventName: string) {
		if (this.eventList.has(eventName)) {
			this.eventList.delete(eventName);
		} else {
			console.info('has not', eventName, 'callback');
		}
	}
	once(eventName: string, callback: (...args: unknown[]) => void) {
		this.on.call(this, eventName, () => {
			callback();
			this.emit(eventName);
			this.off(eventName);
		});
	}
	emit(eventName: string, ...args: any) {
		if (this.eventList.has(eventName)) {
			const eventCallback = this.eventList.get(eventName);
			eventCallback && eventCallback.apply(this, args);
		}
	}
}
enum WebSocketReadyState {
	Connecting = 0,
	Open = 1,
	Closing = 2,
	Closed = 3
}
export enum EventType {
	CONNECTION = 'connection',
	MESSAGE = 'message',
	CLOSED = 'closed',
	PUSH_DATA = 'pushdata',
	SEND_MESSAGE = 'sendmessage',
	HEARTBET = 'heartbeat'
}
export enum SignalType {
	ENTER = 'enter',
	NEW_PEER = 'new_peer',
	ENTER_RESPONSE = 'enter_response',
	LEAVE = 'leave',
	PEER_LEAVE = 'peer_leave',
	CREATE_OFFER = 'createoffer',
	RECV_OFFER = 'recv_offer',
	CREATE_ANSWER = 'createanswer',
	RECV_ANSWER = 'recv_answer',
	ICE_CANDIDATE = 'icecandidate',
	RECV_CANDIDATE = 'recv_candidate',
	LOG = 'log'
}
class MyWebSocket extends EventEmit {
	ws: WebSocket | null;
	readey: boolean;
	clientId: string;
	remoteClientId: string;
	constructor() {
		super();
		this.ws = null;
		this.readey = false;
		this.clientId = shortid();
		this.remoteClientId = '';
	}
	getClientId() {
		return this.clientId;
	}
	getRemoteClientId() {
		return this.remoteClientId;
	}
	setRemoteClientId(remoteClientId: string) {
		this.remoteClientId = remoteClientId;
	}

	startHeartbeat() {
		let time: NodeJS.Timeout | null = null;
		time = setInterval(() => {
			this.send(EventType.HEARTBET, {time: +new Date()});
		}, heartBeatTime);
		return () => {
			time !== null && clearTimeout(time);
			time = null;
		};
	}
	handleError(data: {error?: string}) {
		if (data.error) {
			ElMessage({
				message: data.error,
				type: 'warning'
			});
		} else {
			console.debug(JSON.stringify(data));
		}
	}
	disconnect() {
		if (this.ws) {
			if (this.ws.readyState !== WebSocketReadyState.Closed && this.ws.readyState !== WebSocketReadyState.Closing) {
				this.ws.close();
			}
			this.ws = null;
		}
	}
	getWsUrl() {
		return config.websocketUrl;
	}
	async connect() {
		if (!this.readey) {
			this.readey = true;
		} else {
			return;
		}
		return new Promise<void>((resolve, reject) => {
			const onError = (error: Event | Error | string) => {
				this.startHeartbeat()();
				reject(error);
			};
			if (this.ws && this.ws.readyState === WebSocketReadyState.Open) {
				return Promise.resolve();
			}

			try {
				console.log(this.clientId);
				this.ws = new WebSocket(config.websocketUrl + this.clientId);
				this.ws.onopen = () => {
					console.info('websocket connected');
					this.emit(EventType.CONNECTION);
					this.startHeartbeat();
					resolve();
				};
				this.ws.onerror = function (event): any {
					onError(event);
				};
				this.ws.onmessage = event => {
					this.emit(EventType.MESSAGE, event);
					let data;
					try {
						data = JSON.parse(event.data);
					} catch (e) {
						console.log(`onMessage parse event.data error: ${event.data}`);
						return;
					}
					switch (data.type) {
						case EventType.SEND_MESSAGE:
							return this.emit(EventType.SEND_MESSAGE, data.data);
						case SignalType.NEW_PEER:
							return this.emit(SignalType.NEW_PEER, data.data);
						case SignalType.ENTER_RESPONSE:
							return this.emit(SignalType.ENTER_RESPONSE, data.data);
						case SignalType.PEER_LEAVE:
							return this.emit(SignalType.PEER_LEAVE, data.data);
						case SignalType.RECV_CANDIDATE:
							return this.emit(SignalType.RECV_CANDIDATE, data.data);
						case SignalType.RECV_ANSWER:
							return this.emit(SignalType.RECV_ANSWER, data.data);
						case SignalType.RECV_OFFER:
							console.log('进来了的');
							return this.emit(SignalType.RECV_OFFER, data.data);
						case SignalType.ICE_CANDIDATE:
							return this.emit(SignalType.ICE_CANDIDATE, data.data);
						case SignalType.LOG:
							return this.handleError(data.data);
					}
				};
				this.ws.onclose = async event => {
					console.info(`websocket closed`);
					this.disconnect();
					this.emit(EventType.CLOSED, event);
					this.readey = false;
					this.startHeartbeat()();
					setTimeout(this.connect, 3000);
				};
			} catch (err) {
				reject(err);
			}
		});
	}
	async send(type: string, data = {}, {reqId = ''} = {}) {
		if (this.ws?.readyState !== WebSocketReadyState.Open) {
			await this.connect();
		}
		if (!reqId) {
			reqId = shortid();
		}
		this.ws &&
			this.ws.send(
				JSON.stringify({
					type,
					reqId,
					data
				})
			);
	}
}

export const mywebsocket = new MyWebSocket();
