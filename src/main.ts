import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementPlus, {ElNotification} from 'element-plus';
import '@/assets/scss/index.scss';
const launch = async () => {
	const routerConfig = await router;
	routerConfig.beforeEach(to => {
		if (Number(to.meta.need_auth)) {
			if (to.meta.allow_roles === 'all') return true;
			if (!store.state.userinfo.loginState) {
				ElNotification({
					title: 'Error',
					message: '请先登录哦',
					type: 'warning'
				});
				return false;
			}
		}
	});
	createApp(App).use(ElementPlus).use(store).use(routerConfig).mount('#app');
};
launch();
