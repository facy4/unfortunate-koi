import {ref, watch} from 'vue';

export function useIntersectionObserver(target: HTMLElement | any, callback: IntersectionObserverCallback) {
	const isSupported = ref(window && 'IntersectionObserver' in window);
	let cleanup = () => {
		console.log('--cleanup');
	};
	const stopWatch = isSupported.value
		? watch(
				() => ({
					el: target.value
				}),
				({el}) => {
					if (!el) return;

					const observer = new IntersectionObserver(callback);
					observer.observe(el);
					cleanup = () => {
						observer.disconnect();
						cleanup = () => {
							console.log('--cleanup');
						};
					};
				},
				{immediate: true, flush: 'post'}
		  )
		: () => {
				console.log('--cleanup');
		  };

	const stop = () => {
		cleanup();
		stopWatch();
	};

	return {
		isSupported,
		stop
	};
}
export const lazyimg = {
	// 及他自己的所有子节点都挂载完成后调用
	mounted(el: HTMLImageElement) {
		const observer = new IntersectionObserver(evt => {
			if (evt[0].isIntersecting) {
				el.src = el.alt;
				observer.disconnect();
			}
		});
		observer.observe(el);
	}
};
