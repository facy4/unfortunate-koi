import {onMounted, onUnmounted, Ref, ref} from 'vue';
import {mywebsocket, SignalType} from '@/utils';
import {useCheckWebRTC} from './useCheckWebRTC';
import {ElMessage} from 'element-plus';

export const useConnectWebRTC = ({
	localVideoElement,
	localAudioElement,
	remoteVideoElement,
	remoteAudioElement
}: {
	localVideoElement: Ref<HTMLVideoElement | null>;
	localAudioElement: Ref<HTMLAudioElement | null>;
	remoteVideoElement: Ref<HTMLVideoElement | null>;
	remoteAudioElement: Ref<HTMLAudioElement | null>;
}) => {
	const peerConnection = ref<null | RTCPeerConnection>(null);
	const {
		isSupported,
		streamInstance: myStreamInstance,
		toggleBus,
		stopStream
	} = useCheckWebRTC({
		videoElement: localVideoElement,
		audioElement: localAudioElement
	});
	onMounted(() => {
		toggleBus();
	});
	onUnmounted(() => {
		stopStream();
	});
	const remoteStreamInstance = ref<MediaStream | null>(null);
	// const {streamInstance: otherStreamInstance} = useCheckWebRTC({
	// 	videoElement: remoteVideoElement,
	// 	audioElement: remoteAudioElement
	// });

	const createOffer = (roomId: string) => {
		console.info('createOffer');
		if (!peerConnection.value) {
			createConnnection(roomId);
		}
		peerConnection.value
			?.createOffer()
			.then(session => {
				peerConnection.value?.setLocalDescription(session).then(() => {
					console.log('localoffer', session);
					mywebsocket.send(SignalType.CREATE_OFFER, {
						roomId,
						clientId: mywebsocket.getClientId(),
						remoteClientId: mywebsocket.getRemoteClientId(),
						session: session
					});
					console.info('---', SignalType.CREATE_OFFER);
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
	const createAnswer = (roomId: string) => {
		peerConnection.value
			?.createAnswer()
			.then(session => {
				peerConnection.value?.setLocalDescription(session).then(() => {
					console.log('localAnswer', session);
					mywebsocket.send(SignalType.CREATE_ANSWER, {
						roomId,
						clientId: mywebsocket.getClientId(),
						remoteClientId: mywebsocket.getRemoteClientId(),
						session: session
					});
					console.info('---', SignalType.CREATE_ANSWER);
				});
			})
			.catch(err => {
				console.log(err);
			});
	};
	const handleRomoteOffer = (data: any) => {
		console.info('handleRomoteOffer');
		console.info('remoteoffer', data.session);

		if (!peerConnection.value) {
			createConnnection(data.roomId);
		}
		peerConnection
			.value!.setRemoteDescription(data.session)
			.then(() => {
				createAnswer(data.roomId);
			})
			.catch(err => {
				console.log(err);
			});
	};
	const handleRomoteAnswer = (data: any) => {
		console.log('remoteAnswer', data.session);
		console.info('handleRomoteAnswer', peerConnection.value);
		peerConnection.value!.setRemoteDescription(data.session).catch(err => {
			console.log(err);
		});
	};
	const handleRemoteCandidate = (data: any) => {
		console.info('handleRemoteCandidate');
		peerConnection.value?.addIceCandidate(data.session);
	};
	const handlePeerLEAVE = () => {
		console.info('handlePeerLEAVE');
		if (remoteVideoElement.value) {
			remoteVideoElement.value.srcObject = null;
		}
		if (remoteAudioElement.value) {
			remoteAudioElement.value.srcObject = null;
		}
		remoteStreamInstance.value &&
			remoteStreamInstance.value.getTracks().forEach(track => {
				track.stop();
			});
	};
	const listenEvent = () => {
		mywebsocket.on(SignalType.ENTER, (...evt: any[]) => {
			mywebsocket.send(SignalType.ENTER, {roomId: evt[0].roomId, clientId: mywebsocket.getClientId()});
			ElMessage({
				message: '进入房间成功请等待另一个人加入',
				type: 'success'
			});
		});
		mywebsocket.on(SignalType.LEAVE, (...evt: any[]) => {
			mywebsocket.send(SignalType.LEAVE, {roomId: evt[0].roomId, clientId: mywebsocket.getClientId()});
			handlePeerLEAVE();
			ElMessage({
				message: '离开房间成功',
				type: 'success'
			});
		});
		mywebsocket.on(SignalType.PEER_LEAVE, () => {
			console.info('---', SignalType.PEER_LEAVE);
			handlePeerLEAVE();
		});
		mywebsocket.on(SignalType.ENTER_RESPONSE, (...evt: any[]) => {
			console.info('---', SignalType.ENTER_RESPONSE);
			mywebsocket.setRemoteClientId(evt[0].remoteClientId);
		});
		mywebsocket.on(SignalType.NEW_PEER, (...evt: any[]) => {
			console.info('---', SignalType.NEW_PEER);
			mywebsocket.setRemoteClientId(evt[0].remoteClientId);
			createOffer(evt[0].roomId);
		});
		mywebsocket.on(SignalType.RECV_OFFER, (...evt: any[]) => {
			console.info('---', SignalType.RECV_OFFER);
			handleRomoteOffer(evt[0]);
		});
		mywebsocket.on(SignalType.RECV_ANSWER, (...evt: any[]) => {
			console.info('---', SignalType.RECV_ANSWER);
			handleRomoteAnswer(evt[0]);
		});
		mywebsocket.on(SignalType.RECV_CANDIDATE, (...evt: any[]) => {
			console.info('---', SignalType.RECV_CANDIDATE);
			handleRemoteCandidate(evt[0]);
		});
	};
	const createConnnection = (roomId: string) => {
		console.log(1);
		if (!isSupported.value) {
			throw new Error('不支持webrtc');
		}
		peerConnection.value = new RTCPeerConnection({
			iceServers: [
				{
					urls: ['stun:stun.xten.com', 'stun:stun.voipbuster.com', 'stun:stun.voxgratia.org']
				}
			]
		});
		peerConnection.value.onicecandidate = function (evt) {
			console.info('---onicecandidate');
			if (evt.candidate) {
				mywebsocket.send(SignalType.ICE_CANDIDATE, {
					roomId,
					clientId: mywebsocket.getClientId(),
					remoteClientId: mywebsocket.getRemoteClientId(),
					session: evt.candidate
				});
			}
		};
		peerConnection.value.ontrack = function (evt) {
			console.info('---ontrack');
			remoteStreamInstance.value = evt.streams[0];
			if (remoteVideoElement.value && remoteStreamInstance.value) {
				remoteVideoElement.value.srcObject = remoteStreamInstance.value;
			}
		};
		myStreamInstance.value?.getTracks().forEach(track => {
			if (myStreamInstance.value) {
				console.log(myStreamInstance, track);
				peerConnection.value!.addTrack(track, myStreamInstance.value);
			}
		});
	};
	onMounted(() => {
		listenEvent();
	});
};
