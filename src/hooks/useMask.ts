import {createApp} from 'vue';
const maskMap = new Map();
export const useMask = ({component, mountElId}: {component: any; mountElId: string}) => {
	const test = createApp(component);
	const maskContainerReady = () => {
		return new Promise<void>(resolve => {
			if (maskMap.has(mountElId)) {
				return resolve();
			}
			const newContainer = document.createElement('div');
			newContainer.id = mountElId;
			maskMap.set(mountElId, newContainer);
			document.body.appendChild(newContainer);
			resolve();
		});
	};
	const createMask = () => {
		maskContainerReady().then(() => {
			test.mount(`#${mountElId}`);
		});
	};
	const hideMask = () => {
		maskMap.get(mountElId).classList.add('hide-element');
		document.documentElement.classList.remove('disabled');
	};
	const showMask = () => {
		if (!maskMap.has(mountElId)) {
			createMask();
		} else {
			maskMap.get(mountElId).classList.remove('hide-element');
		}
		document.documentElement.classList.add('disabled');
	};
	const destroyMask = () => {
		if (maskMap.has(mountElId)) {
			document.body.removeChild(maskMap.get(mountElId));
			maskMap.delete(mountElId);
		}
	};
	return {createMask, hideMask, showMask, destroyMask};
};
