import {nextTick, reactive, Ref, ref, watch} from 'vue';

export const useCheckWebRTC = ({
	videoElement,
	audioElement
}: {
	videoElement: Ref<HTMLVideoElement | null>;
	audioElement: Ref<HTMLAudioElement | null>;
}) => {
	function isWebRTCSupported() {
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		return !!(navigator.mediaDevices?.getUserMedia && window.RTCPeerConnection);
	}
	const isSupported = ref(isWebRTCSupported());
	const isCameraAvailable = ref(false);
	const isMicrophoneAvailable = ref(false);
	const isMuted = ref(false);
	const streamInstance = ref<null | MediaStream>(null);
	const cameraDevices = reactive<MediaDeviceInfo[]>([]);
	const bus = ref(false);
	const toggleMute = () => {
		if (streamInstance.value) {
			const audioTracks = streamInstance.value.getAudioTracks();
			isMuted.value = !isMuted.value;
			audioTracks.forEach(track => {
				track.enabled = !isMuted.value;
			});
		}
	};
	const stopStream = () => {
		streamInstance.value &&
			streamInstance.value.getTracks().forEach(track => {
				track.stop();
			});
	};
	const getDevices = async () => {
		try {
			const deviceInfos = await navigator.mediaDevices.enumerateDevices();
			cameraDevices.length = 0;
			deviceInfos.forEach(deviceInfo => {
				if (deviceInfo.kind === 'videoinput') {
					cameraDevices.push(deviceInfo);
				}
			});
		} catch (err) {
			console.error('Failed to get device list:', err);
		}
	};
	const getStream = () => {
		navigator.mediaDevices
			.getUserMedia({
				video: {
					width: 640,
					height: 480
				},
				audio: true
			})
			.then(stream => {
				streamInstance.value = stream;
				isCameraAvailable.value = stream.getVideoTracks().length > 0;
				isMicrophoneAvailable.value = stream.getAudioTracks().length > 0;
				nextTick().then(() => {
					if (videoElement.value && isCameraAvailable.value) {
						videoElement.value.srcObject = stream;
						videoElement.value.play();
					}
					if (audioElement.value && isMicrophoneAvailable.value) {
						audioElement.value.srcObject = stream;
						audioElement.value.play();
					}
				});
			})
			.catch(error => {
				console.error(error);
			});
	};
	const toggleBus = () => {
		bus.value = !bus.value;
	};
	watch(
		() => bus.value,
		val => {
			if (val) {
				getDevices();
				getStream();
			} else {
				stopStream();
			}
		},
		{immediate: true}
	);
	return {
		isSupported,
		isCameraAvailable,
		isMicrophoneAvailable,
		isMuted,
		cameraDevices,
		toggleMute,
		stopStream,
		streamInstance,
		toggleBus,
		bus
	};
};
