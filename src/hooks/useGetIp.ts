import querystring from 'querystring';
import {ref} from 'vue';
import {useStore} from 'vuex';

export const useGetIp = () => {
	const store = useStore();
	const ipConfigRef = ref<any>(null);
	const insertScript = () => {
		try {
			if (ipConfigRef.value) return;
			(window as any).getIpConfig = (ipConfig: any) => {
				const {city, ip, addr} = ipConfig;
				ipConfigRef.value = ipConfig;
				store.commit('setIpConfigInfo', {city, ip, addr});
			};
			const script = document.createElement('script');
			script.type = 'text/javascript';
			script.onerror = () => {
				console.log('err load');
			};
			const params = {
				callback: 'getIpConfig'
			};
			script.src = `http://whois.pconline.com.cn/ipJson.jsp?callback=getIpConfig${querystring.stringify(params)}`;
			document.body.appendChild(script);
		} catch (err) {
			console.error(err);
		}
	};

	return [ipConfigRef.value, insertScript];
};
