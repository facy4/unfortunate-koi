import {ProcessState} from '@/constants/global';
import {ref, watch} from 'vue';
import {useDynamicload} from './useDynamicload';

export const useLoadCKEditor = () => {
	const {state, instance} = useDynamicload({
		globalName: 'ClassicEditor',
		url: 'https://unfortunate-koi-1307188191.cos.ap-guangzhou.myqcloud.com/ckeditor.js ',
		id: 'ClassicEditor'
	});
	const editorInstance = ref();
	const editLoadState = ref(ProcessState.Pendding);
	watch(
		() => state.value,
		val => {
			if (val === ProcessState.Fullfilled) {
				instance.value
					.create(document.querySelector('#editor'), {
						image: {
							toolbar: [
								'imageStyle:inline',
								'imageStyle:block',
								'imageStyle:side',
								'|',
								'toggleImageCaption',
								'imageTextAlternative'
							]
						},
						toolbar: {
							items: [
								'bold',
								'link',
								'|',
								'undo',
								'redo',
								'|',
								'Indent',
								'outdent',
								'|',
								'codeblock',
								'|',
								'imageUpload',
								'insertTable'
							]
						},
						simpleUpload: {
							// The URL that the images are uploaded to.
							uploadUrl: '/api/accumulation/upload',
							// Enable the XMLHttpRequest.withCredentials property.
							withCredentials: true
							// Headers sent along with the XMLHttpRequest to the upload server.
						}
					})
					.then((editor: any) => {
						editorInstance.value = editor;
						editLoadState.value = ProcessState.Fullfilled;
					})
					.catch((error: any) => {
						console.error('There was a problem initializing the editor.', error);
					});
			}
		}
	);
	return {
		editorInstance,
		state: editLoadState
	};
};
