import {ProcessState} from '@/constants/global';
import {ref} from 'vue';
import {WindowEx} from '@/constants/global';
function LoadScript(options: {id: string; src: string; onload?: () => void; onerror?: () => void}) {
	const el = document.getElementById(options.id);
	if (el) {
		return;
	}
	const script = document.createElement('script');
	if (options.onload) {
		script.onload = options.onload;
	}
	if (options.onerror) {
		script.onerror = options.onerror;
	}
	script.src = options.src;
	document.body.appendChild(script);
}

export const useDynamicload = ({globalName, url, id}: {globalName: keyof typeof WindowEx; url: string; id: string}) => {
	const instance = ref();
	const state = ref(ProcessState.Pendding);
	const startLoad = (globalName: keyof typeof WindowEx, url: string, id: string) => {
		if (globalName in window) {
			return Promise.resolve(window[globalName as any]);
		}
		return new Promise<any>((resolve, reject) => {
			LoadScript({
				id,
				src: url,
				onload: () => {
					if (globalName in window) {
						resolve(window[globalName as any]);
						state.value = ProcessState.Fullfilled;
					}
				},
				onerror: () => {
					state.value = ProcessState.Rejected;
					reject();
				}
			});
		});
	};
	startLoad(globalName, url, id).then(res => {
		instance.value = res;
		state.value = ProcessState.Fullfilled;
	});
	return {
		instance,
		state
	};
};
