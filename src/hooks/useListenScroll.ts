import {onMounted, onUnmounted, ref} from 'vue';
export const useListenScroll = () => {
	const fixedDialog = ref(false);
	onMounted(() => {
		document.body.onscroll = () => {
			fixedDialog.value = document.documentElement.scrollTop > 0;
		};
	});
	onUnmounted(() => {
		window.onscroll = null;
	});

	return {
		fixedDialog
	};
};
