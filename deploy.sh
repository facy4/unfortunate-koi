#!/bin/bash
echo "删除之前文件"
rm -rf /usr/projects/unfortunate-koi/dist
pnpm install && pnpm run build
echo "开始构建镜像"
docker build -t unfortunate-koi .
echo "删除旧容器"
docker stop unfortunate-koi-container
docker rm unfortunate-koi-container
echo "启动新容器"
docker container run -p 80:80 -p 443:443 -d --name unfortunate-koi-container unfortunate-koi

