FROM nginx
LABEL name="unfortunate-koi"
LABEL version="1.0"
COPY  ./dist/ /usr/share/nginx/html/
COPY ./unfortunate-koi.conf /etc/nginx/conf.d/
COPY ./facy.top.key  /etc/nginx
COPY ./facy.top_bundle.crt  /etc/nginx
EXPOSE 80 443
