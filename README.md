# unfortunate-koi by chaos_fhm

### 个人网站功能开发一览

- 登录鉴权功能:white_check_mark:

- 基础管理模块api管理user管理router管理:white_check_mark:

- 富文本编辑器CKEditor5已接入，文章列表发步功能:white_check_mark:

- ui重构
  - 首页 :frog:
    - 基本布局  :white_check_mark:
    - 动态接入 :four:
  - 菜单导航栏 :frog:
    - 基本布局j:white_check_mark:
  - 基础组件 :frog:
    - formTable改造完成:white_check_mark:
    - useMask添加:white_check_mark:

- 文章列表，发布 :five:
  - 添加文章
  - 文章列表

- im通讯WebRTC

  - 添加websocket​简单​实现​im:white_check_mark:
  - WebRTC自检功能:white_check_mark:
  - WebRtc打洞可以做到p2p连接:white_check_mark:
  - 完成WebRtc打洞TURN服务器没钱:white_check_mark:
  - 后续添加临时用户身份:white_check_mark:

- 杂项
  - loading操作 :white_check_mark:
  - 超时处理:white_check_mark:
  - 404处理 :white_check_mark:
  - 报错重发:white_check_mark:
  - 网站升级https:white_check_mark:
  - 增加日志错误捕获上报:four:

- 计划功能
  1. 首页管理模块，动态配置首页轮播图及展示内容；:four:
  2. 文章审核功能:three:
  3. 爬点喜欢内容放上去, 写点东西:two:

- 已知bug

  1、用户第一次登录时候加密iv对不上:white_check_mark:

  2、表单默认值有问题:white_check_mark:

