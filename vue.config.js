const CompressionPlugin = require('compression-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');
const cdns = {
  dev: {
    css: ["//unpkg.com/element-plus/dist/index.css", '//unpkg.com/element-plus/theme-chalk/display.css', '//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css'],
    js: []
  },
  prod: {
    css: ["//unpkg.com/element-plus/dist/index.css", '//unpkg.com/element-plus/theme-chalk/display.css', '//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css'],
    js: [
      'https://unpkg.com/vue@3.2.12/dist/vue.global.prod.js',
      'https://unpkg.com/element-plus@2.2.12/dist/index.full.min.js'
    ]
  }
};
const isProd = process.env.NODE_ENV === 'production';
module.exports = {
  publicPath: "/",
  configureWebpack: (config) => {
    if (isProd) {
      config.plugins.push(new CompressionPlugin({
        algorithm: 'gzip', // 使用gzip压缩
        test: /\.js$|\.html$|\.css$/, // 匹配文件名
        filename: '[path].gz[query]', // 压缩后的文件名(保持原文件名，后缀加.gz)
        minRatio: 1, // 压缩率小于1才会压缩
        threshold: 10240, // 对超过10k的数据压缩
        deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
      }));
      config.externals = {
        'vue': 'Vue',
        "element-plus": "ElementPlus",
      };
      config.plugins.push(
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: false,
          terserOptions: {
            ecma: undefined,
            warning: false,
            parse: {},
            compress: {
              drop_console: true,
              drop_debugger: true,
              pure_funcs: [//移除console
                'console.log'
              ]
            }
          }
        }));
    }
    config.resolve = {
      extensions: ['.ts', '.vue', '.tsx', '.json', '.js'],
      alias: {
        "@": path.resolve(__dirname, "./src"),
      }
    };
  },
  chainWebpack: config => {
    config.plugin("html").tap(args => {
      const [options] = args;
      options.cdns = isProd ? cdns.prod : cdns.dev;
      return args;
    });
  },
  devServer: {
    port: "8888",
    host: '0.0.0.0',
    proxy: {
      "/api": {
        target: "http://192.168.1.102:9998", // 后台接口域名
        changeOrigin: true, //是否跨域
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
  productionSourceMap: false,
};
